FROM golang:1.17.7-bullseye as build
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -o app.out

FROM alpine:3.15.0 as runner
RUN apk --update add libc6-compat
COPY --from=build /app/app.out /usr/local/bin/app
EXPOSE 3000
CMD [ "app" ]
