package repositories_test

import (
	"database/sql"
	"regexp"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type MockUser struct {
	ID              uuid.UUID
	Created_at      time.Time
	Updated_at      time.Time
	Deleted_at      time.Time
	Validated_at    time.Time
	Name            string
	Email           string
	Password        string
	ValidationToken string
}

var hash, _ = helpers.HashPassword("password")
var validationToken = helpers.RandSeq(6)

var u = MockUser{Name: "Test", Email: "test@example.com", Password: hash, ValidationToken: validationToken}

var _ = Describe("UserRepository", func() {
	var userRepo *repositories.UserRepository
	var mock sqlmock.Sqlmock

	BeforeEach(func() {
		var db *sql.DB
		var err error

		db, mock, err = sqlmock.New() // mock sql.DB
		Expect(err).ShouldNot(HaveOccurred(), "Error mock sql DB")

		dialector := postgres.New(postgres.Config{
			DSN:                  "sqlmock_db_0",
			DriverName:           "postgres",
			Conn:                 db,
			PreferSimpleProtocol: true,
		})

		gdb, err := gorm.Open(dialector, &gorm.Config{SkipDefaultTransaction: true}) // open gorm db
		Expect(err).ShouldNot(HaveOccurred(), "Error open gorm DB")

		ur := repositories.NewUserRepository(gdb)
		userRepo = &ur
	})
	AfterEach(func() {
		err := mock.ExpectationsWereMet() // make sure all expectations were met
		Expect(err).ShouldNot(HaveOccurred(), "Expectations were not met")
	})

	Context("save", func() {
		var user *MockUser
		BeforeEach(func() {
			user = &MockUser{
				Name:            u.Name,
				Email:           u.Email,
				Password:        u.Password,
				ValidationToken: u.ValidationToken,
			}
		})

		It("create", func() {

			user, _ := userRepo.Create(user.Name, user.Email, user.Password, user.ValidationToken)

			Expect(user.Name).Should(BeEquivalentTo(u.Name), "UserRepo create return wrong name")
			Expect(user.Email).Should(BeEquivalentTo(u.Email), "UserRepo create return wrong email")
			Expect(user.Password).Should(BeEquivalentTo(u.Password), "UserRepo create return wrong password")
			Expect(user.ValidationToken).Should(BeEquivalentTo(u.ValidationToken), "UserRepo create return wrong validation token")
		})

		It("update", func() {
			user, _ := userRepo.Create(user.Name, user.Email, user.Password, user.ValidationToken)
			Expect(user.Name).Should(BeEquivalentTo(u.Name), "UserRepo create return wrong name")
			Expect(user.Email).Should(BeEquivalentTo(u.Email), "UserRepo create return wrong email")
			Expect(user.Password).Should(BeEquivalentTo(u.Password), "UserRepo create return wrong password")
			Expect(user.ValidationToken).Should(BeEquivalentTo(u.ValidationToken), "UserRepo create return wrong validation token")

			user.Name = "Changed"
			user, _ = userRepo.UpdateOne(user.ID, &user)
			Expect(user.Name).Should(BeEquivalentTo("Changed"), "UserRepo update return wrong name")
			Expect(user.Email).Should(BeEquivalentTo(u.Email), "UserRepo update return wrong email")
			Expect(user.Password).Should(BeEquivalentTo(u.Password), "UserRepo update return wrong password")
		})
	})

	Context("find by email", func() {
		It("found", func() {
			rows := sqlmock.
				NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "email", "password", "validation_token", "validated_at"}).
				AddRow(uuid.New().String(), time.Now(), time.Now(), nil, u.Name, u.Email, u.Password, u.ValidationToken, nil)

			const sqlSearch = `
				SELECT * FROM "users"
				WHERE email = $1 AND "users"."deleted_at" IS NULL
				ORDER BY "users"."id" LIMIT 1`

			mock.ExpectQuery(regexp.QuoteMeta(sqlSearch)).
				WithArgs(u.Email).
				WillReturnRows(rows)

			l, err := userRepo.FindByEmail(u.Email)
			Expect(err).ShouldNot(HaveOccurred(), "UserRepo find by email error")

			Expect(l.Name).Should(BeEquivalentTo(u.Name), "UserRepo find by email return wrong name")
			Expect(l.Email).Should(BeEquivalentTo(u.Email), "UserRepo find by email return wrong email")
			Expect(l.Password).Should(BeEquivalentTo(u.Password), "UserRepo find by email return wrong password")
		})

		It("not found", func() {
			mock.ExpectQuery(`.+`).WillReturnRows(sqlmock.NewRows(nil))
			_, err := userRepo.FindByEmail(u.Email)
			Expect(err).Should(Equal(gorm.ErrRecordNotFound))
		})
	})
})
