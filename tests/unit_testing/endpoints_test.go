package endpoints_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gorm.io/gorm"
	"net/http"
	"os"
	"testing"
	"time"
)

type User struct {
	ID              uuid.UUID
	Created_at      time.Time
	Updated_at      time.Time
	Deleted_at      time.Time
	Validated_at    time.Time
	Name            string
	Email           string
	Password        string
	ValidationToken string
}

var u = User{Name: "Test", Email: "shared.custom.address@gmail.com", Password: "password"}

var hash, _ = helpers.HashPassword(u.Password)

func prepareDbTesting(t *testing.T) (*gorm.DB, string, string) {
	if err := godotenv.Load("../../.env"); err != nil {
		t.Fatal("Error loading .env")
	}

	port := os.Getenv("PORT")
	prodMode := os.Getenv("ENV") == "production"
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	db, err := helpers.ConnectDb(dbHost, dbPort, dbUser, dbPass, dbName, !prodMode)
	if err != nil {
		errMsg := "Error connecting to database"
		return nil, "", errMsg
	}

	var user User
	result := db.Where("email = ?", u.Email).First(&user)

	if result.Error == nil {
		result := db.Unscoped().Where("email LIKE ?", u.Email).Delete(User{})

		if result.Error != nil {
			errMsg := "Error deleting existing account"
			return nil, "", errMsg
		}
	}

	return db, port, ""
}

func TestRegister(t *testing.T) {
	t.Skip("Temporaryily disabled")
	_, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	postBody, _ := json.Marshal(map[string]string{
		"Name":     u.Name,
		"Email":    u.Email,
		"Password": u.Password,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/register", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusCreated)
	}
}

func TestRegisterError(t *testing.T) {
	t.Skip("Temporaryily disabled")
	db, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	var user = User{Name: u.Name, Email: u.Email, Password: hash}
	result := db.Create(&user)
	if result.Error != nil {
		t.Fatal("Error creating test user")
	}

	postBody, _ := json.Marshal(map[string]string{
		"Name":     u.Name,
		"Email":    u.Email,
		"Password": u.Password,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/register", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusInternalServerError)
	}
}

func dbRegisterTestAccount(t *testing.T, db *gorm.DB, port string) {
	t.Skip("Temporaryily disabled")
	postBody, _ := json.Marshal(map[string]string{
		"Name":     u.Name,
		"Email":    u.Email,
		"Password": u.Password,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/register", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusCreated)
	}
}

func dbValidateTestAccount(t *testing.T, db *gorm.DB, port string) {
	t.Skip("Temporaryily disabled")
	var user User
	result := db.Where("email = ?", u.Email).First(&user)
	if result.Error != nil {
		t.Fatal(result.Error)
	}

	postBody, _ := json.Marshal(map[string]string{
		"ValidationToken": user.ValidationToken,
		"Email":           u.Email,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/validate", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusOK)
	}
}

func TestValidate(t *testing.T) {
	t.Skip("Temporaryily disabled")
	db, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	dbRegisterTestAccount(t, db, port)

	dbValidateTestAccount(t, db, port)
}

func TestLogin(t *testing.T) {
	t.Skip("Temporaryily disabled")
	db, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	dbRegisterTestAccount(t, db, port)

	dbValidateTestAccount(t, db, port)

	postBody, _ := json.Marshal(map[string]string{
		"Email":    u.Email,
		"Password": u.Password,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/login", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusOK)
	}
}

func TestLoginInvalidEmail(t *testing.T) {
	t.Skip("Temporaryily disabled")
	_, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	postBody, _ := json.Marshal(map[string]string{
		"Email":    u.Email,
		"Password": u.Password,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/login", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusNotFound)
	}
}

func TestLoginInvalidPassword(t *testing.T) {
	t.Skip("Temporaryily disabled")
	db, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	dbRegisterTestAccount(t, db, port)

	dbValidateTestAccount(t, db, port)

	postBody, _ := json.Marshal(map[string]string{
		"Email":    u.Email,
		"Password": "wrongPassword",
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/login", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusBadRequest)
	}
}

func TestForgotPassword(t *testing.T) {
	t.Skip("Temporaryily disabled")
	db, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	dbRegisterTestAccount(t, db, port)

	dbValidateTestAccount(t, db, port)

	postBody, _ := json.Marshal(map[string]string{
		"Email": u.Email,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/request-reset-password", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusCreated)
	}
}

func TestForgotPasswordInvalidEmail(t *testing.T) {
	t.Skip("Temporaryily disabled")
	_, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	postBody, _ := json.Marshal(map[string]string{
		"Email": u.Email,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/request-reset-password", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusNotFound)
	}
}

func TestForgotPasswordErrorRepetitiveRequest(t *testing.T) {
	t.Skip("Temporaryily disabled")
	db, port, errMsg := prepareDbTesting(t)
	if errMsg != "" {
		t.Errorf("DB error: %s", errMsg)
	}

	dbRegisterTestAccount(t, db, port)

	dbValidateTestAccount(t, db, port)

	postBody, _ := json.Marshal(map[string]string{
		"Email": u.Email,
	})
	responseBody := bytes.NewBuffer(postBody)
	url := fmt.Sprintf("http://localhost:%s/user/auth/request-reset-password", port)
	resp, err := http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusCreated)
	}

	responseBody = bytes.NewBuffer(postBody)
	resp, err = http.Post(url, "application/json", responseBody)
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if status := resp.StatusCode; status != http.StatusNonAuthoritativeInfo {
		t.Errorf("handler returned wrong status code: got %v want %v", resp.StatusCode, http.StatusNonAuthoritativeInfo)
	}
}
