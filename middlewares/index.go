package middlewares

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
)

func Setup(app *fiber.App, prodMode bool) {
	app.Use(logger.New(logger.Config{
		TimeZone:   "UTC",
		TimeFormat: "2006-01-02 15:04:05",
	}))
	app.Use(recover.New(recover.Config{
		EnableStackTrace: !prodMode,
	}))
}

//Middleware untuk protect route
//endpoint : /auth
//payload :
func IsAuthorized(c *fiber.Ctx) error {
	token := c.GetReqHeaders()["Authorization"]

	if len(strings.Split(token, " ")) <= 1 {
		result, _ := helpers.GenerateResponse(nil, "Anda belum login!")
		return c.Status(401).SendString(string(result))
	}

	token = strings.Split(token, " ")[1]

	tokenData, err := helpers.ParseJWT(token)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, string(err.Error()))
		return c.Status(401).SendString(string(result))
	}

	if err != nil {
		panic(err)
	}

	c.Locals("user", tokenData)
	return c.Next()
}

func IsAuthorizedQ(c *fiber.Ctx) error {
	token := c.Query("token")

	if len(strings.Split(token, " ")) <= 0 {
		result, _ := helpers.GenerateResponse(nil, "Anda belum login!")
		return c.Status(401).SendString(string(result))
	}

	tokenData, err := helpers.ParseJWT(token)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, string(err.Error()))
		return c.Status(401).SendString(string(result))
	}

	if err != nil {
		panic(err)
	}

	c.Locals("user", tokenData)
	return c.Next()
}
