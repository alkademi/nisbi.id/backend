package modules

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/controller"
)

type exampleModule struct {
	exampleController controller.ExampleController
}

func newExampleModule(app *fiber.App) exampleModule {
	ctrl := controller.NewExampleController(app)
	return exampleModule{
		exampleController: ctrl,
	}
}
