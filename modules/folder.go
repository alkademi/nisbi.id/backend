package modules

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/controller"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/usecase"
	"gorm.io/gorm"
)

type folderModule struct {
	folderRepository   repositories.FileRepository
	folderController   controller.FolderController
	favoriteRepository repositories.FavoriteRepository
}

func newFolderModule(app *fiber.App, db *gorm.DB) (folderModule, error) {
	if err := db.AutoMigrate(&models.File{}); err != nil {
		return folderModule{}, err
	}
	folderRepo := repositories.NewFileRepository(db)
	fileUsecase := usecase.NewFileUseCase(folderRepo)
	folderUsecase := usecase.NewFolderUseCase(folderRepo)
	favoriteRepo := repositories.NewFavoriteRepository(db)
	folderController := controller.NewFolderController(
		app,
		folderRepo,
		fileUsecase,
		folderUsecase,
		favoriteRepo,
	)
	return folderModule{
		folderRepository:   folderRepo,
		folderController:   folderController,
		favoriteRepository: favoriteRepo,
	}, nil
}
