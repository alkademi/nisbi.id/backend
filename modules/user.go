package modules

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/controller"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gorm.io/gorm"
)

type userModule struct {
	userRepository repositories.UserRepository
	fptRepository  repositories.ForgotPasswordTokenRepository
	userController controller.UserController
}

func newUserModule(app *fiber.App, db *gorm.DB) (userModule, error) {
	if err := db.AutoMigrate(&models.User{}); err != nil {
		return userModule{}, err
	}
	repo := repositories.NewUserRepository(db)
	fptRepo := repositories.NewForgotPasswordTokenRepository(db)
	return userModule{
		userRepository: repo,
		fptRepository:  fptRepo,
		userController: controller.NewUserController(app, repo, fptRepo),
	}, nil
}
