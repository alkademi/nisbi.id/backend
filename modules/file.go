package modules

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/controller"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/usecase"
	"gorm.io/gorm"
)

type fileModule struct {
	fileRepository     repositories.FileRepository
	favoriteRepository repositories.FavoriteRepository
	fileController     controller.FileController
}

func newFileModule(app *fiber.App, db *gorm.DB) (fileModule, error) {
	if err := db.AutoMigrate(&models.File{}); err != nil {
		return fileModule{}, err
	}
	fileRepo := repositories.NewFileRepository(db)
	fileUsecase := usecase.NewFileUseCase(fileRepo)
	favRepo := repositories.NewFavoriteRepository(db)
	fileController := controller.NewFileController(app, fileRepo, favRepo, fileUsecase)
	return fileModule{
		fileRepository:     fileRepo,
		favoriteRepository: favRepo,
		fileController:     fileController,
	}, nil
}
