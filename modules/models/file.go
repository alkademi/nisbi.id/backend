package models

import (
	"github.com/google/uuid"
)

type File struct {
	BaseNoDeletedAt
	Name           string     `gorm:"type:VARCHAR(255);not null" json:"name"`
	UserID         uuid.UUID  `gorm:"type:uuid;not null" json:"user_id"`
	Size           int64      `gorm:"not null" json:"size"`
	MimeType       string     `gorm:"type:VARCHAR(100)" json:"mime_type"`
	IsShared       bool       `gorm:"not null" json:"is_shared"`
	Url            string     `gorm:"not null" json:"url"`
	ParentFolderID *uuid.UUID `gorm:"type:uuid" json:"parent_folder_id"`
	IsFolder       bool       `gorm:"not null" json:"is_folder"`
	SavedFileName  string     `gorm:"VARCHAR(255);not null" json:"saved_file_name"`
	// TODO: Confirm this will not break
	Parent *File `gorm:"foreignKey:ParentFolderID" json:"parent"`
	User   User  `gorm:"foreignKey:UserID" json:",omitempty"`
}
