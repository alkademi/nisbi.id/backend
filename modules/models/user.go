package models

import (
	"time"
)

type User struct {
	Base
	Name                 string                `gorm:"type:VARCHAR(255);not null" json:"name"`
	Email                string                `gorm:"type:VARCHAR(255);not null;uniqueIndex" json:"email"`
	Password             string                `gorm:"type:VARCHAR(255);not null" json:"password"`
	ValidationToken      string                `gorm:"type:VARCHAR(255);not null" json:"validation_token"`
	ValidatedAt          time.Time             `gorm:"default:null" json:"validated_at"`
	Files                []File                `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"files"`
	Favorites            []Favorite            `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"favorites"`
	ForgotPasswordTokens []ForgotPasswordToken `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"forgot_password_tokens"`
}
