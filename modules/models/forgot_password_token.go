package models

import (
	"github.com/google/uuid"
)

type ForgotPasswordToken struct {
	Base
	Value  string    `gorm:"type:varchar(100);not null" json:"value"`
	UserId uuid.UUID `gorm:"type:uuid;not null;index" json:"user_id"`
}
