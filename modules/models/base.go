package models

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// A copy of gorm.Model but with UUID for ID and default value for CreatedAt and UpdatedAt
type Base struct {
	ID        uuid.UUID      `gorm:"type:uuid;primary_key;default:UUID_GENERATE_V4()" json:"id"`
	CreatedAt time.Time      `gorm:"default:NOW();not null" json:"created_at"`
	UpdatedAt time.Time      `gorm:"default:NOW();not null" json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}

// A copy of gorm.Model but with UUID for ID and default value for CreatedAt and UpdatedAt
type BaseNoDeletedAt struct {
	ID        uuid.UUID `gorm:"type:uuid;primary_key;default:UUID_GENERATE_V4()" json:"id"`
	CreatedAt time.Time `gorm:"default:NOW();not null" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:NOW();not null" json:"updated_at"`
}
