package models

import (
	"github.com/google/uuid"
)

type Favorite struct {
	BaseNoDeletedAt
	UserID uuid.UUID `gorm:"type:uuid;not null;uniqueIndex:idx_user_file" json:"user_id"`
	FileID uuid.UUID `gorm:"type:uuid;not null;uniqueIndex:idx_user_file" json:"file_id"`
	File   File      `gorm:"constraint:OnUpdate:CASCADE;constraint:OnDelete:CASCADE" json:"file"`
}
