package controller

import (
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/middlewares"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
)

type UserController struct {
	userRepo repositories.UserRepository
	fptRepo  repositories.ForgotPasswordTokenRepository
}

type ResetPasswordPayload struct {
	Token    string
	Password string
}

type ValidateEmailPayload struct {
	Email           string
	ValidationToken string
}

type AuthRes struct {
	Token string `json:"token"`
}

func NewUserController(
	app *fiber.App,
	userRepo repositories.UserRepository,
	fptRepo repositories.ForgotPasswordTokenRepository,
) UserController {
	router := app.Group("/user")

	ur := UserController{
		userRepo: userRepo,
		fptRepo:  fptRepo,
	}

	router.Get("/auth", middlewares.IsAuthorized, ur.validateUser)
	router.Post("/auth/register", ur.registerHandler)
	router.Post("/auth/login", ur.loginHandler)
	router.Post("/auth/relogin", ur.reloginHandler)
	router.Post("/auth/request-reset-password", ur.requestResetPasswordHandler)
	router.Post("/auth/reset-password", ur.resetPasswordHandler)
	router.Post("/auth/validate", ur.validateEmail)

	return ur
}

//Fungsi untuk validasi user
//endpoint : /auth
//payload :
func (controller *UserController) validateUser(c *fiber.Ctx) error {
	userData := helpers.GetUserData(c)
	result, _ := helpers.GenerateResponse(userData, "")

	return c.Status(200).SendString(string(result))
}

//Fungsi untuk handle register
//endpoint : /auth/register
//payload : Name, Email, Password
func (controller *UserController) registerHandler(c *fiber.Ctx) error {
	user := new(models.User)
	if err := c.BodyParser(&user); err != nil {
		panic(err)
	}

	res, err := controller.userRepo.FindByEmail(user.Email)
	if err == nil {
		result, _ := helpers.GenerateResponse(nil, "Pengguna dengan email tersebut sudah ada!")
		return c.Status(400).SendString(string(result))
	}

	hash, _ := helpers.HashPassword(user.Password)
	validationToken := helpers.RandSeq(6)

	res, err = controller.userRepo.Create(user.Name, user.Email, hash, validationToken)
	if err != nil {
		panic(err)
	}

	err = helpers.SendValidationEmail(validationToken, user.Email)
	if err != nil {
		panic(err)
	}

	token, err := helpers.GenerateJWT(res)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "Terjadi kesalahan saat pengiriman email validasi!")
		return c.Status(400).SendString(string(result))
	}

	resData := new(AuthRes)
	resData.Token = token

	result, err := helpers.GenerateResponse(resData, "Pendaftaran pengguna berhasil, harap cek email untuk validasi akun anda!")
	if err != nil {
		panic(err)
	}

	return c.Status(201).SendString(string(result))
}

func (controller *UserController) reloginHandler(c *fiber.Ctx) error {
	token := c.GetReqHeaders()["Authorization"]

	// if len(strings.Split(token, " ")) <= 1 {
	// 	result, _ := helpers.GenerateResponse(nil, "Anda belum login!")
	// 	return c.Status(401).SendString(string(result))
	// }

	token = strings.Split(token, " ")[1]

	userData, err := helpers.ParseJWT(token)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, string(err.Error()))
		return c.Status(401).SendString(string(result))
	}

	if err != nil {
		panic(err)
	}

	userId := userData.Id
	user, err := controller.userRepo.FindByID(userId)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "User tidak ditemukan")
		return c.Status(404).SendString(string(result))
	}

	token, err = helpers.GenerateJWT(user)

	if err != nil {
		panic(err)
	}

	resData := new(AuthRes)
	resData.Token = token

	response, err := helpers.GenerateResponse(resData, "")
	if err != nil {
		panic(err)
	}

	return c.Status(200).SendString(string(response))
}

//Fungsi untuk handle login
//endpoint : /auth/login
//payload : Email, Password
func (controller *UserController) loginHandler(c *fiber.Ctx) error {
	payload := new(models.User)
	if err := c.BodyParser(&payload); err != nil {
		panic(err)
	}

	user, err := controller.userRepo.FindByEmail(payload.Email)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "User tidak ditemukan")
		return c.Status(404).SendString(string(result))
	}

	if valid_password := helpers.CheckPassword(payload.Password, user.Password); valid_password == false {
		result, _ := helpers.GenerateResponse(nil, "Surel atau sandi tidak valid!")
		return c.Status(400).SendString(string(result))
	}

	if user.ValidatedAt.IsZero() {
		result, _ := helpers.GenerateResponse(user, "Akun ini belum diaktivasi!")
		return c.Status(403).SendString(string(result))
	}

	token, err := helpers.GenerateJWT(user)
	if err != nil {
		panic(err)
	}

	resData := new(AuthRes)
	resData.Token = token

	response, err := helpers.GenerateResponse(resData, "Login Berhasil!")
	if err != nil {
		panic(err)
	}

	return c.Status(200).SendString(string(response))
}

//Fungsi untuk menginisiasi perubahan password
//endpoint : /auth/request-reset-password
//payload : Email
func (controller *UserController) requestResetPasswordHandler(c *fiber.Ctx) error {
	payload := new(models.User)
	if err := c.BodyParser(&payload); err != nil {
		panic(err)
	}

	user, err := controller.userRepo.FindByEmail(payload.Email)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "Pengguna tidak ditemukan!")
		return c.Status(404).SendString(string(result))
	}

	fpt, err := controller.fptRepo.FindByUserID(user.ID)
	if err == nil {
		err = helpers.SendResetPasswordToken(fpt.Value, payload.Email)
		result, _ := helpers.GenerateResponse(nil, "Token untuk pengubahan sandi telah dikirimkan ulang ke email anda")
		return c.Status(203).SendString(string(result))
	}

	token := helpers.RandSeq(6)
	fpt, err = controller.fptRepo.Create(token, user.ID)

	err = helpers.SendResetPasswordToken(fpt.Value, payload.Email)
	if err != nil {
		panic(err)
	}

	result, err := helpers.GenerateResponse(nil, "Email token perubahan sandi berhasil terkirim!")
	return c.Status(201).SendString(string(result))
}

//Fungsi untuk handle reset password
//endpoint : /auth/reset-password
//payload : Token, Password
func (controller *UserController) resetPasswordHandler(c *fiber.Ctx) error {
	payload := new(ResetPasswordPayload)
	if err := c.BodyParser(&payload); err != nil {
		panic(err)
	}

	fpt, err := controller.fptRepo.FindByToken(payload.Token)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "Token tidak valid!")
		return c.Status(404).SendString(string(result))
	}

	user, err := controller.userRepo.FindByID(fpt.UserId)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "Pengguna tidak ditemukan!")
		return c.Status(404).SendString(string(result))
	}

	hash, _ := helpers.HashPassword(payload.Password)
	user.Password = hash

	_, err = controller.fptRepo.DeleteOne(fpt.ID)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "Terjadi kesalahan saat menebus token!"+err.Error())
		return c.Status(404).SendString(string(result))
	}

	_, err = controller.userRepo.UpdateOne(user.ID, &user)
	if err != nil {
		panic(err)
	}

	result, err := helpers.GenerateResponse(nil, "Sandi berhasil diganti!")
	return c.Status(200).SendString(string(result))
}

//Fungsi untuk handle validasi akun
//endpoint : /auth/validate
//payload : ValidationToken, Email
func (controller *UserController) validateEmail(c *fiber.Ctx) error {
	payload := new(ValidateEmailPayload)
	if err := c.BodyParser(&payload); err != nil {
		panic(err)
	}

	user, err := controller.userRepo.FindByEmail(payload.Email)
	if err != nil {
		result, _ := helpers.GenerateResponse(nil, "Pengguna tidak ditemukan!")
		return c.Status(404).SendString(string(result))
	}

	if user.ValidationToken != payload.ValidationToken {
		result, _ := helpers.GenerateResponse(nil, "Token validasi tidak valid!")
		return c.Status(403).SendString(string(result))
	}

	if !user.ValidatedAt.IsZero() {
		result, _ := helpers.GenerateResponse(nil, "Email sudah tervalidasi!")
		return c.Status(400).SendString(string(result))
	}

	user.ValidatedAt = time.Now()
	_, err = controller.userRepo.UpdateOne(user.ID, &user)
	if err != nil {
		panic(err)
	}

	result, err := helpers.GenerateResponse(nil, "Email berhasil divalidasi!")
	return c.Status(200).SendString(string(result))
}
