package controller

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
)

type ExampleController struct{}

func NewExampleController(app *fiber.App) ExampleController {
	router := app.Group("/")

	router.Get("/ping", pingHandler)

	return ExampleController{}
}

func pingHandler(c *fiber.Ctx) error {
	res, err := helpers.GenerateResponse(nil, "pong")
	if err != nil {
		panic(err)
	}
	return c.Status(418).SendString(string(res))
}
