package controller

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
)

type ForgotPasswordTokenController struct {
	forgotPasswordTokenRepo repositories.ForgotPasswordTokenRepository
}

func NewForgotPasswordTokenController(
	app *fiber.App,
	forgotPasswordTokenRepo repositories.ForgotPasswordTokenRepository,
) ForgotPasswordTokenController {
	fptr := ForgotPasswordTokenController{
		forgotPasswordTokenRepo: forgotPasswordTokenRepo,
	}

	return fptr
}
