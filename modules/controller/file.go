package controller

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/middlewares"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/usecase"
)

type FileController struct {
	fileRepo     repositories.FileRepository
	fileUc       usecase.FileUseCase
	favoriteRepo repositories.FavoriteRepository
}

type NewFileResponse struct {
	Failed  []string       `json:"failed"`
	Success []*models.File `json:"success"`
}

type EditFilePayload struct {
	Name     *string `json:"name,omitempty"`
	IsShared *bool   `json:"is_shared,omitempty"`
	ParentId *string `json:"parent_id,omitempty"`
}

type FileResponse struct {
	ID             uuid.UUID  `gorm:"type:uuid;primary_key;" json:"id"`
	Name           string     `gorm:"type:VARCHAR(255);not null" json:"name"`
	Size           int64      `gorm:"not null" json:"size"`
	MimeType       string     `gorm:"type:VARCHAR(100)" json:"mime_type"`
	IsShared       bool       `gorm:"not null" json:"is_shared"`
	Url            string     `gorm:"not null" json:"url"`
	ParentFolderID *uuid.UUID `gorm:"type:uuid" json:"parent_folder_id"`
	IsFolder       bool       `gorm:"not null" json:"is_folder"`
	SavedFileName  string     `gorm:"VARCHAR(255);not null" json:"saved_file_name"`
	IsFavorite     *bool      `json:"is_favorite,omitempty"`
	OwnerName      *string    `json:"owner_name,omitempty"`
	UserId         *uuid.UUID `gorm:"type:uuid" json:"user_id"`
	CreatedAt      time.Time  `json:"created_at"`
	UpdatedAt      time.Time  `json:"updated_at"`
}

func NewFileController(
	app *fiber.App,
	fileRepo repositories.FileRepository,
	favoriteRepo repositories.FavoriteRepository,
	fileUc usecase.FileUseCase,
) FileController {
	fCtrl := FileController{
		fileRepo:     fileRepo,
		favoriteRepo: favoriteRepo,
		fileUc:       fileUc,
	}
	r := app.Group("/files")

	r.Get("/:id/download", middlewares.IsAuthorizedQ, fCtrl.serveFile)

	r.Use(middlewares.IsAuthorized)
	r.Post("/", fCtrl.newFile)
	r.Get("/", fCtrl.fileList)
	r.Get("/:id", fCtrl.fileData)
	r.Put("/:id", fCtrl.editFile)
	r.Delete("/:id", fCtrl.deleteFile)
	r.Post("/:id/copy", fCtrl.copyFile)

	return fCtrl
}

func (ctrlr FileController) newFile(c *fiber.Ctx) error {
	godotenv.Load()
	form, err := c.MultipartForm()
	if err != nil {
		return err
	}
	files := form.File["files"]
	var parentIdForm string
	if val, ok := form.Value["parent_id"]; ok {
		parentIdForm = val[0]
	} else {
		parentIdForm = ""
	}

	var parentId *uuid.UUID
	if parentIdForm == "" {
		parentId = nil
	} else {
		tmp, _ := uuid.Parse(parentIdForm)
		parentId = &tmp
	}

	uploadDir := os.Getenv("UPLOAD_DIR")
	if uploadDir == "" {
		uploadDir = "./uploads"
	}

	userId := helpers.GetUserData(c).Id
	failedSavesName, successfulSaves, c_err := ctrlr.fileUc.Create(c, parentId, files, userId, uploadDir)

	if c_err != nil {
		res, _ := helpers.GenerateResponse(c_err.Data, c_err.Message)
		return c.Status(c_err.Code).SendString(string(res))
	}

	respData := NewFileResponse{
		Failed:  failedSavesName,
		Success: successfulSaves,
	}

	if len(failedSavesName) == 0 {
		res, _ := helpers.GenerateResponse(respData, "successfuly uploaded all file(s)")
		return c.Status(fiber.StatusCreated).SendString(string(res))
	}

	if len(successfulSaves) == 0 {
		res, _ := helpers.GenerateResponse(respData, "failed uploading all file(s)")
		return c.Status(fiber.StatusInternalServerError).SendString(string(res))
	}

	res, _ := helpers.GenerateResponse(respData, "failed uploading some file(s)")
	return c.Status(fiber.StatusMultiStatus).SendString(string(res))
}

func (ctrlr FileController) serveFile(c *fiber.Ctx) error {
	godotenv.Load()
	uploadDir := os.Getenv("UPLOAD_DIR")
	if uploadDir == "" {
		uploadDir = "./uploads"
	}
	id := c.Params("id")
	uuid, _ := uuid.Parse(id)
	userId := helpers.GetUserData(c).Id

	path, err := ctrlr.fileUc.GetPath(&uuid, &userId, uploadDir)

	if err != nil {
		resp, _ := helpers.GenerateResponse(nil, err.Message)
		return c.Status(err.Code).SendString(string(resp))
	}

	return c.Status(fiber.StatusOK).SendFile(path, true)
}

func (ctrlr FileController) fileData(c *fiber.Ctx) error {
	userId := helpers.GetUserData(c).Id
	id := c.Params("id")
	fileId, _ := uuid.Parse(id)

	file, err := ctrlr.fileRepo.GetFileById(&fileId)
	if err != nil || (file.UserID != userId && !file.IsShared) {
		resp, _ := helpers.GenerateResponse(nil, "Not found")
		return c.Status(fiber.StatusNotFound).SendString(string(resp))
	}

	favorites, err := ctrlr.favoriteRepo.GetFavoritesByUserId(userId, false)
	IsFavorite := false
	for _, fav := range favorites {
		if fav.FileID == file.ID {
			IsFavorite = true
			break
		}
	}

	resp, _ := helpers.GenerateResponse(FileResponse{
		ID:             file.ID,
		Name:           file.Name,
		Size:           file.Size,
		MimeType:       file.MimeType,
		IsShared:       file.IsShared,
		Url:            file.Url,
		ParentFolderID: file.ParentFolderID,
		IsFolder:       file.IsFolder,
		SavedFileName:  file.SavedFileName,
		IsFavorite:     &IsFavorite,
		OwnerName:      &file.User.Name,
		UserId:         &file.UserID,
		CreatedAt:      file.CreatedAt,
		UpdatedAt:      file.UpdatedAt,
	}, "")

	return c.Status(fiber.StatusOK).SendString(string(resp))
}

func (ctrlr FileController) fileList(c *fiber.Ctx) error {
	userId := helpers.GetUserData(c).Id
	searchQuery := c.Query("q")

	filesCombined, err := ctrlr.fileRepo.GetByUserAndParentId(&userId, nil, searchQuery)
	if err != nil {
		resp, _ := helpers.GenerateResponse(nil, "Tidak ada berkas")
		return c.Status(fiber.StatusOK).SendString(string(resp))
	}

	files := make([]FileResponse, 0)
	folders := make([]FileResponse, 0)

	favorites, err := ctrlr.favoriteRepo.GetFavoritesByUserId(userId, false)

	for _, file := range *filesCombined {
		IsFavorite := false

		for _, fav := range favorites {
			if fav.FileID == file.ID {
				IsFavorite = true
				break
			}
		}

		temp := FileResponse{
			ID:             file.ID,
			Name:           file.Name,
			Size:           file.Size,
			MimeType:       file.MimeType,
			IsShared:       file.IsShared,
			Url:            file.Url,
			ParentFolderID: file.ParentFolderID,
			IsFolder:       file.IsFolder,
			SavedFileName:  file.SavedFileName,
			IsFavorite:     &IsFavorite,
			OwnerName:      &file.User.Name,
			CreatedAt:      file.CreatedAt,
			UpdatedAt:      file.UpdatedAt,
		}

		if file.IsFolder {
			folders = append(folders, temp)
		} else {
			files = append(files, temp)
		}
	}

	resp, _ := helpers.GenerateResponse(map[string][]FileResponse{
		"files":   files,
		"folders": folders,
	}, "")

	return c.Status(fiber.StatusOK).SendString(string(resp))
}

func (ctrlr FileController) editFile(c *fiber.Ctx) error {
	userId := helpers.GetUserData(c).Id
	id := c.Params("id")
	fileId, _ := uuid.Parse(id)

	file, err := ctrlr.fileRepo.GetFileById(&fileId)
	if err != nil || file.UserID != userId {
		resp, _ := helpers.GenerateResponse(nil, "Tidak ada berkas")
		return c.Status(fiber.StatusNotFound).SendString(string(resp))
	}

	payload := new(EditFilePayload)
	if err = c.BodyParser(&payload); err != nil {
		resp, _ := helpers.GenerateResponse(nil, "Body tidak valid!")
		return c.Status(fiber.StatusBadRequest).SendString(string(resp))
	}

	if payload.ParentId != nil {
		if *payload.ParentId != "root" {
			tmp, _ := uuid.Parse(*payload.ParentId)
			parentId := &tmp
			_, err := helpers.ValidateFolder(ctrlr.fileRepo, parentId, userId)
			if err != nil {
				resp, _ := helpers.GenerateResponse(err, "Folder tidak valid")
				return c.Status(fiber.StatusBadRequest).SendString(string(resp))
			}
			file.ParentFolderID = parentId
		} else {
			file.ParentFolderID = nil
		}

	}

	if payload.Name != nil {
		file.Name = *payload.Name
	}

	if payload.IsShared != nil {
		file.IsShared = *payload.IsShared
		if *payload.IsShared == false {
			ctrlr.favoriteRepo.ResetThirdPersonFavorites(userId, file.ID)
		}

		if file.IsFolder {
			err = helpers.BulkToggleShare(ctrlr.fileRepo, &file.ID, &userId, *payload.IsShared)
			if err != nil {
				panic(err)
			}
		}
	}

	_, err = ctrlr.fileRepo.UpdateOne(fileId, file)
	if err != nil {
		panic(err)
	}

	resp, _ := helpers.GenerateResponse(nil, "File berhasil diubah!")
	return c.Status(fiber.StatusOK).SendString(string(resp))
}

func (ctrlr FileController) deleteFile(c *fiber.Ctx) error {
	godotenv.Load()
	uploadDir := os.Getenv("UPLOAD_DIR")
	if uploadDir == "" {
		uploadDir = "./uploads"
	}

	id := c.Params("id")
	userId := helpers.GetUserData(c).Id
	uuid, _ := uuid.Parse(id)

	err := ctrlr.fileUc.Delete(uuid, userId, uploadDir, false)

	if err != nil {
		resp, _ := helpers.GenerateResponse(err.Data, err.Message)
		return c.Status(err.Code).SendString(string(resp))
	}

	return c.SendStatus(fiber.StatusNoContent)
}

func (ctrlr FileController) copyFile(c *fiber.Ctx) error {
	userId := helpers.GetUserData(c).Id
	id := c.Params("id")
	fileId, _ := uuid.Parse(id)

	godotenv.Load()
	uploadDir := os.Getenv("UPLOAD_DIR")
	if uploadDir == "" {
		uploadDir = "./uploads"
	}

	file, err := ctrlr.fileRepo.GetFileById(&fileId)

	if err != nil || (file.UserID != userId && !file.IsShared) {
		resp, _ := helpers.GenerateResponse(nil, "Anda tidak punya hak untuk mengcopy file ini!")
		return c.Status(fiber.StatusUnauthorized).SendString(string(resp))
	}

	if file.IsFolder {
		resp, _ := helpers.GenerateResponse(nil, "Folder tidak dapat dicopy!")
		return c.Status(fiber.StatusPreconditionFailed).SendString(string(resp))
	}

	original, err := os.Open(uploadDir + "/" + file.SavedFileName)
	if err != nil {
		resp, _ := helpers.GenerateResponse(nil, "Gagal membuka file")
		return c.Status(fiber.StatusInternalServerError).SendString(string(resp))
	}
	defer original.Close()

	targetName, err := helpers.SanitizeFileName(ctrlr.fileRepo, file.ParentFolderID, &userId, file.Name, true)

	// creating target file
	originalName := string(file.SavedFileName[20:len(file.SavedFileName)])
	newSavedFileName := fmt.Sprintf("%d_%s", time.Now().UnixNano(), originalName)
	new, err := os.Create(uploadDir + "/" + newSavedFileName)
	if err != nil {
		resp, _ := helpers.GenerateResponse(nil, "Gagal membuat copy-an file")
		return c.Status(fiber.StatusInternalServerError).SendString(string(resp))
	}
	defer new.Close()

	// copying file
	_, err = io.Copy(new, original)
	if err != nil {
		os.Remove(fmt.Sprintf("%s/%s", uploadDir, newSavedFileName))
		resp, _ := helpers.GenerateResponse(nil, "Gagal membuat copy-an file")
		return c.Status(fiber.StatusInternalServerError).SendString(string(resp))
	}

	newID, _ := uuid.NewUUID()
	newURL := fmt.Sprintf("/files/%s", newID.String())
	newFile, err := ctrlr.fileRepo.Create(targetName, file.MimeType, newURL, newSavedFileName, file.Size, file.IsFolder, &newID, &userId, file.ParentFolderID)

	if err != nil {
		os.Remove(fmt.Sprintf("%s/%s", uploadDir, newSavedFileName))
		resp, _ := helpers.GenerateResponse(nil, "Gagal membuat copy-an file")
		return c.Status(fiber.StatusInternalServerError).SendString(string(resp))
	}

	resp, _ := helpers.GenerateResponse(newFile, "File berhasil diduplikasi dengan nama "+targetName+" !")
	return c.Status(fiber.StatusCreated).SendString(string(resp))
}
