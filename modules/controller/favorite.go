package controller

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/middlewares"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
)

type FavoriteController struct {
	favoriteRepo repositories.FavoriteRepository
}

func NewFavoriteController(
	app *fiber.App,
	favoriteRepo repositories.FavoriteRepository,
) FavoriteController {
	router := app.Group("/favorite")

	favoriteController := FavoriteController{
		favoriteRepo: favoriteRepo,
	}

	router.Use(middlewares.IsAuthorized)
	router.Get("/", favoriteController.getFavorites)
	router.Post("/:id", favoriteController.toggleFavorites)

	return favoriteController
}

func (controller *FavoriteController) getFavorites(c *fiber.Ctx) error {
	userData := helpers.GetUserData(c)
	favorites, err := controller.favoriteRepo.GetFavoritesByUserId(userData.Id, true)

	if err != nil {
		panic(err)
	}

	result, _ := helpers.GenerateResponse(favorites, "")
	return c.Status(200).SendString(string(result))
}

func (controller *FavoriteController) toggleFavorites(c *fiber.Ctx) error {
	userId := helpers.GetUserData(c).Id
	fileId := uuid.MustParse(c.Params("id"))
	favorite, err := controller.favoriteRepo.GetSingleFavorite(userId, fileId)

	if err == nil {
		result, err := controller.favoriteRepo.DeleteOne(favorite.ID)
		if err != nil {
			result, _ := helpers.GenerateResponse(result, "Terjadi kesalahan saat menghapus favorite! "+err.Error())
			return c.Status(500).SendString(string(result))
		}
		res, _ := helpers.GenerateResponse(result, "Favorit berhasil dihapus!")
		return c.Status(200).SendString(string(res))
	}

	result, err := controller.favoriteRepo.Create(userId, fileId)
	if err != nil {
		panic(err)
	}
	res, _ := helpers.GenerateResponse(result, "Favorit berhasil ditambahkan!")
	return c.Status(200).SendString(string(res))
}
