package controller

import (
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/middlewares"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/usecase"
)

type FolderController struct {
	fileRepo     repositories.FileRepository
	fileUc       usecase.FileUseCase
	folderUc     usecase.FolderUseCase
	favoriteRepo repositories.FavoriteRepository
}

func NewFolderController(
	app *fiber.App,
	fileRepo repositories.FileRepository,
	fileUc usecase.FileUseCase,
	folderUc usecase.FolderUseCase,
	favoriteRepo repositories.FavoriteRepository,
) FolderController {
	fCtrl := FolderController{
		fileRepo:     fileRepo,
		fileUc:       fileUc,
		folderUc:     folderUc,
		favoriteRepo: favoriteRepo,
	}
	r := app.Group("/folders")

	r.Use(middlewares.IsAuthorized)
	r.Get("/", fCtrl.listFolders)
	r.Post("/", fCtrl.create)
	r.Get("/:id", fCtrl.listFolders)
	r.Delete("/:id", fCtrl.deleteFolder)
	r.Get("/:id/download", fCtrl.downloadFolder)

	return fCtrl
}

func (ctrlr FolderController) create(c *fiber.Ctx) error {
	body := models.File{}
	var out map[string]string
	var parent_id *uuid.UUID
	var err error
	c.BodyParser(&out)
	if out["parent_folder_id"] == "" {
		parent_id = nil
	} else {
		var tmp uuid.UUID
		tmp, err = uuid.Parse(out["parent_folder_id"])
		if err != nil {
			resp, _ := helpers.GenerateResponse(nil, "Parent ID bukan UUID valid")
			return c.Status(fiber.StatusBadRequest).SendString(string(resp))
		}
		parent_id = &tmp
	}

	body.Name = out["name"]
	body.ParentFolderID = parent_id

	userId := helpers.GetUserData(c).Id
	if body.ParentFolderID != nil {
		var parent *models.File
		parent, err = ctrlr.fileRepo.GetFileById(body.ParentFolderID)
		if err != nil {
			resp, _ := helpers.GenerateResponse(nil, "Parent tidak ditemukan")
			return c.Status(fiber.StatusNotFound).SendString(string(resp))
		}
		if parent.IsFolder == false {
			resp, _ := helpers.GenerateResponse(nil, "Parent bukan folder")
			return c.Status(fiber.StatusPreconditionFailed).SendString(string(resp))
		}
	}
	if len(body.Name) > 255 || len(body.Name) <= 0 {
		resp, _ := helpers.GenerateResponse(nil, "Nama file terlalu besar atau terlalu kecil")
		return c.Status(fiber.StatusRequestEntityTooLarge).SendString(string(resp))
	}

	folderId, _ := uuid.NewUUID()

	folderName, err := helpers.SanitizeFileName(ctrlr.fileRepo, body.ParentFolderID, &userId, body.Name, false)

	if err != nil {
		resp, _ := helpers.GenerateResponse(err, "")
		return c.Status(fiber.StatusInternalServerError).SendString(string(resp))
	}

	var folder *models.File
	if folder, err = ctrlr.fileRepo.Create(
		folderName,
		"application/x-folder",
		fmt.Sprintf("/folders/%s", folderId.String()),
		"",
		0,
		true,
		&folderId,
		&userId,
		body.ParentFolderID,
	); err != nil {
		resp, _ := helpers.GenerateResponse(nil, "Gagal membuat folder")
		return c.Status(fiber.StatusInternalServerError).SendString(string(resp))
	}

	resp, _ := helpers.GenerateResponse(folder, "Berhasil membuat folder")
	return c.Status(fiber.StatusCreated).SendString(string(resp))
}

func (ctrlr FolderController) listFolders(c *fiber.Ctx) error {
	userId := helpers.GetUserData(c).Id
	parentIdStr := c.Params("id")
	var parentId *uuid.UUID
	if parentIdStr == "" {
		parentId = nil
	} else {
		parentIdTmp, _ := uuid.Parse(parentIdStr)
		parentId = &parentIdTmp

		file, err := ctrlr.fileRepo.GetFileById(parentId)
		if err != nil || (!file.IsFolder) {
			resp, _ := helpers.GenerateResponse(nil, "Not found")
			return c.Status(fiber.StatusNotFound).SendString(string(resp))
		}
	}
	searchQuery := c.Query("q")

	filesCombined, err := ctrlr.fileRepo.GetByUserAndParentId(&userId, parentId, searchQuery)
	if err != nil {
		resp, _ := helpers.GenerateResponse(nil, "Tidak ada berkas")
		return c.Status(fiber.StatusOK).SendString(string(resp))
	}

	files := make([]FileResponse, 0)
	folders := make([]FileResponse, 0)

	favorites, err := ctrlr.favoriteRepo.GetFavoritesByUserId(userId, false)

	for _, file := range *filesCombined {
		IsFavorite := false

		for _, fav := range favorites {
			if fav.FileID == file.ID {
				IsFavorite = true
				break
			}
		}

		temp := FileResponse{
			ID:             file.ID,
			Name:           file.Name,
			Size:           file.Size,
			MimeType:       file.MimeType,
			IsShared:       file.IsShared,
			Url:            file.Url,
			ParentFolderID: file.ParentFolderID,
			IsFolder:       file.IsFolder,
			SavedFileName:  file.SavedFileName,
			IsFavorite:     &IsFavorite,
			OwnerName:      &file.User.Name,
			UserId:         &file.UserID,
			CreatedAt:      file.CreatedAt,
			UpdatedAt:      file.UpdatedAt,
		}

		if file.IsFolder {
			folders = append(folders, temp)
		} else {
			files = append(files, temp)
		}
	}

	resp, _ := helpers.GenerateResponse(map[string][]FileResponse{
		"files":   files,
		"folders": folders,
	}, "")

	return c.Status(fiber.StatusOK).SendString(string(resp))
}

func (ctrlr FolderController) deleteFolder(c *fiber.Ctx) error {
	godotenv.Load()
	uploadDir := os.Getenv("UPLOAD_DIR")
	if uploadDir == "" {
		uploadDir = "./uploads"
	}

	id := c.Params("id")
	userId := helpers.GetUserData(c).Id
	uuid, _ := uuid.Parse(id)

	ctrlr.fileUc.Delete(uuid, userId, uploadDir, true)

	return c.SendStatus(fiber.StatusNoContent)
}

func (ctrlr FolderController) downloadFolder(c *fiber.Ctx) error {
	godotenv.Load()
	uploadDir := os.Getenv("UPLOAD_DIR")
	tempDir := os.Getenv("TMP_DIR")

	parentIdTmp := c.Params("id")
	parentId, _ := uuid.Parse(parentIdTmp)
	userId := helpers.GetUserData(c).Id
	fname, err := ctrlr.folderUc.Zip(&parentId, &userId, tempDir, uploadDir)

	if err != nil {
		resp, _ := helpers.GenerateResponse(nil, err.Message)
		return c.Status(err.Code).SendString(string(resp))
	}

	defer os.Remove(fname)

	return c.SendFile(fname, true)
}
