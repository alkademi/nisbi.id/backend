package usecase_test

import (
	"database/sql"
	"fmt"
	"io/fs"
	"net/http"
	"os"
	"regexp"
	"time"

	"github.com/google/uuid"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/usecase"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func prepareSelectFileQuery(mock *sqlmock.Sqlmock, file *models.File) {
	(*mock).
		ExpectQuery(
			regexp.QuoteMeta(`SELECT * FROM "files" WHERE id = $1 ORDER BY "files"."id" LIMIT 1`),
		).
		WithArgs(file.ID).
		WillReturnRows(sqlmock.NewRows([]string{
			"id",
			"created_at",
			"updated_at",
			"deleted_at",
			"name",
			"user_id",
			"size",
			"mime_type",
			"is_shared",
			"url",
			"parent_folder_id",
			"is_folder",
			"saved_file_name",
		}).AddRow(
			file.ID,
			time.Now(),
			time.Now(),
			nil,
			file.Name,
			file.UserID,
			file.Size,
			file.MimeType,
			file.IsShared,
			file.Url,
			file.ParentFolderID,
			file.IsFolder,
			file.SavedFileName,
		))
}

var tmpFolderName = ".__tmp_test"
var userId = uuid.New()
var mockRootFile = models.File{
	BaseNoDeletedAt: models.BaseNoDeletedAt{
		ID: uuid.New(),
	},
	Name:           "test.txt",
	Size:           1,
	UserID:         userId,
	MimeType:       "text/plain",
	IsShared:       false,
	Url:            "http://example.com/test.txt",
	ParentFolderID: nil,
	IsFolder:       false,
	SavedFileName:  "test.txt",
}
var mockRootFolder = models.File{
	BaseNoDeletedAt: models.BaseNoDeletedAt{
		ID: uuid.New(),
	},
	Name:           "test-folder",
	Size:           0,
	UserID:         userId,
	MimeType:       "application/x-folder",
	IsShared:       false,
	Url:            "",
	ParentFolderID: nil,
	IsFolder:       true,
	SavedFileName:  "",
}
var mockChildFile = models.File{
	BaseNoDeletedAt: models.BaseNoDeletedAt{
		ID: uuid.New(),
	},
	Name:           "child.txt",
	Size:           1,
	UserID:         userId,
	MimeType:       "text/plain",
	IsShared:       false,
	Url:            "http://example.com/test.txt",
	ParentFolderID: &mockRootFolder.ID,
	IsFolder:       false,
	SavedFileName:  "",
}
var mockChildFolder = models.File{
	BaseNoDeletedAt: models.BaseNoDeletedAt{
		ID: uuid.New(),
	},
	Name:           "child-folder",
	Size:           0,
	UserID:         userId,
	MimeType:       "application/x-folder",
	IsShared:       false,
	Url:            "",
	ParentFolderID: &mockRootFolder.ID,
	IsFolder:       true,
	SavedFileName:  "",
}

var _ = Describe("FileUseCase", func() {
	var fileRepo *repositories.FileRepository
	var fileUC *usecase.FileUseCase
	var mock sqlmock.Sqlmock

	BeforeEach(func() {
		var db *sql.DB
		var err error

		db, mock, err = sqlmock.New() // mock sql.DB
		Expect(err).ShouldNot(HaveOccurred(), "Error mock sql DB")

		dialector := postgres.New(postgres.Config{
			DSN:                  "sqlmock_db_0",
			DriverName:           "postgres",
			Conn:                 db,
			PreferSimpleProtocol: true,
		})

		gdb, err := gorm.Open(dialector, &gorm.Config{SkipDefaultTransaction: true}) // open gorm db
		Expect(err).ShouldNot(HaveOccurred(), "Error open gorm DB")

		ur := repositories.NewFileRepository(gdb)
		uc := usecase.NewFileUseCase(ur)
		fileRepo = &ur
		fileUC = &uc

		err = os.Mkdir(tmpFolderName, fs.ModePerm)
		Expect(err).ShouldNot(HaveOccurred(), "Can't create temporary folder")
	})

	AfterEach(func() {
		err := mock.ExpectationsWereMet() // make sure all expectations were met
		Expect(err).ShouldNot(HaveOccurred(), "Expectations were not met")
		err = os.RemoveAll(tmpFolderName)
		Expect(err).ShouldNot(HaveOccurred(), "Can't remove temporary folder")
	})

	Context("Create File", func() {
		It("It should create a root file", func() {
			mock.
				ExpectQuery(regexp.QuoteMeta(`
					INSERT INTO "files"
						("name","user_id","size","mime_type","is_shared","url","parent_folder_id","is_folder","saved_file_name","id")
					VALUES
						($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)
					RETURNING "id","created_at","updated_at"
				`)).
				WithArgs(
					mockRootFile.Name,
					mockRootFile.UserID,
					mockRootFile.Size,
					mockRootFile.MimeType,
					mockRootFile.IsShared,
					mockRootFile.Url,
					mockRootFile.ParentFolderID,
					mockRootFile.IsFolder,
					mockRootFile.SavedFileName,
					mockRootFile.ID,
				).
				WillReturnRows(sqlmock.NewRows([]string{
					"name",
					"created_at",
					"updated_at",
				}).AddRow(
					mockRootFile.ID,
					time.Now(),
					time.Now(),
				))

			file, err := fileRepo.Create(
				mockRootFile.Name,
				mockRootFile.MimeType,
				mockRootFile.Url,
				mockRootFile.SavedFileName,
				mockRootFile.Size,
				mockRootFile.IsFolder,
				&mockRootFile.ID,
				&mockRootFile.UserID,
				mockRootFile.ParentFolderID,
			)
			Expect(err).ShouldNot(HaveOccurred(), "Error create root file")
			Expect(file.ID).Should(Equal(mockRootFile.ID))
			// INFO: we don't check for name because for some reason, gorm changes the name to its ID
			// Expect(file.Name).Should(Equal(mockRootFile.Name))
			Expect(file.MimeType).Should(Equal(mockRootFile.MimeType))
			Expect(file.Url).Should(Equal(mockRootFile.Url))
			Expect(file.SavedFileName).Should(Equal(mockRootFile.SavedFileName))
			Expect(file.Size).Should(Equal(mockRootFile.Size))
			Expect(file.IsFolder).Should(Equal(mockRootFile.IsFolder))
			Expect(file.ParentFolderID).Should(Equal(mockRootFile.ParentFolderID))
			Expect(file.UserID).Should(Equal(mockRootFile.UserID))
		})
	})

	Context("Invalid file/folder deletions", func() {
		It("should return HTTP not found (404)", func() {
			mock.
				ExpectQuery(
					regexp.QuoteMeta(`SELECT * FROM "files" WHERE id = $1 ORDER BY "files"."id" LIMIT 1`),
				).
				WithArgs(uuid.Nil).
				WillReturnError(sql.ErrNoRows)
			err := fileUC.Delete(uuid.Nil, uuid.Nil, "", false)
			Expect(err).Should(HaveOccurred())
			Expect(err.Code).Should(Equal(http.StatusNotFound))
		})
		It("should return HTTP Unauthorized (401)", func() {
			prepareSelectFileQuery(&mock, &mockRootFile)
			err := fileUC.Delete(mockRootFile.ID, uuid.Nil, "", false)
			Expect(err).Should(HaveOccurred())
			Expect(err.Code).Should(Equal(http.StatusUnauthorized))
		})
		It("should return HTTP precondition failed (412)", func() {
			prepareSelectFileQuery(&mock, &mockRootFolder)
			err := fileUC.Delete(mockRootFolder.ID, mockRootFolder.UserID, "", false)
			Expect(err).Should(HaveOccurred())
			Expect(err.Code).Should(Equal(http.StatusPreconditionFailed))
		})
		It("should return HTTP internal server error (500)", func() {
			prepareSelectFileQuery(&mock, &mockRootFile)
			err := fileUC.Delete(mockRootFile.ID, mockRootFile.UserID, "", true)
			Expect(err).Should(HaveOccurred())
			Expect(err.Code).Should(Equal(http.StatusInternalServerError))
		})
	})

	Context("Valid file/folder deletions", func() {
		It("should delete the file and remove it from DB", func() {
			_, err := os.Create(fmt.Sprintf("%s/%s", tmpFolderName, mockRootFile.SavedFileName))
			Expect(err).ShouldNot(HaveOccurred(), "Cannot create temporary file")

			prepareSelectFileQuery(&mock, &mockRootFile)
			mock.ExpectExec(regexp.QuoteMeta(`DELETE FROM "files" WHERE "files"."id" = $1`)).
				WithArgs(mockRootFile.ID).
				WillReturnResult(sqlmock.NewResult(1, 1))
			err = fileUC.Delete(mockRootFile.ID, mockChildFile.UserID, tmpFolderName, false)
			Expect(err).ShouldNot(HaveOccurred(), "Error delete root file")
		})

		It("should delete the folder and its children, also deletes from DB", func() {
			_, err := os.Create(fmt.Sprintf("%s/%s", tmpFolderName, mockChildFile.SavedFileName))
			prepareSelectFileQuery(&mock, &mockRootFolder)
			mock.
				ExpectQuery(
					regexp.QuoteMeta(`
						SELECT *
						FROM "files"
						WHERE user_id = $1
							AND parent_folder_id = $2
					`),
				).
				WithArgs(mockRootFolder.UserID, mockRootFolder.ID).
				WillReturnRows(sqlmock.NewRows([]string{
					"id",
					"created_at",
					"updated_at",
					"deleted_at",
					"name",
					"user_id",
					"size",
					"mime_type",
					"is_shared",
					"url",
					"parent_folder_id",
					"is_folder",
					"saved_file_name",
				}).AddRow(
					mockChildFile.ID,
					time.Now(),
					time.Now(),
					nil,
					mockChildFile.Name,
					mockChildFile.UserID,
					mockChildFile.Size,
					mockChildFile.MimeType,
					mockChildFile.IsShared,
					mockChildFile.Url,
					mockChildFile.ParentFolderID,
					mockChildFile.IsFolder,
					mockChildFile.SavedFileName,
				).AddRow(
					mockChildFolder.ID,
					time.Now(),
					time.Now(),
					nil,
					mockChildFolder.Name,
					mockChildFolder.UserID,
					mockChildFolder.Size,
					mockChildFolder.MimeType,
					mockChildFolder.IsShared,
					mockChildFolder.Url,
					mockChildFolder.ParentFolderID,
					mockChildFolder.IsFolder,
					mockChildFolder.SavedFileName,
				))

			prepareSelectFileQuery(&mock, &mockChildFile)
			mock.ExpectExec(regexp.QuoteMeta(`DELETE FROM "files" WHERE "files"."id" = $1`)).
				WithArgs(mockChildFile.ID).
				WillReturnResult(sqlmock.NewResult(1, 1))

			prepareSelectFileQuery(&mock, &mockChildFolder)
			mock.
				ExpectQuery(
					regexp.QuoteMeta(`
						SELECT *
						FROM "files"
						WHERE user_id = $1
							AND parent_folder_id = $2
					`),
				).
				WithArgs(mockChildFolder.UserID, mockChildFolder.ID).
				WillReturnRows(sqlmock.NewRows([]string{}))
			mock.ExpectExec(regexp.QuoteMeta(`DELETE FROM "files" WHERE "files"."id" = $1`)).
				WithArgs(mockChildFolder.ID).
				WillReturnResult(sqlmock.NewResult(1, 1))

			mock.ExpectExec(regexp.QuoteMeta(`DELETE FROM "files" WHERE "files"."id" = $1`)).
				WithArgs(mockRootFolder.ID).
				WillReturnResult(sqlmock.NewResult(1, 1))

			err = fileUC.Delete(mockRootFolder.ID, mockRootFolder.UserID, tmpFolderName, true)
			Expect(err).ShouldNot(HaveOccurred(), "Error delete root file")
		})
	})
})
