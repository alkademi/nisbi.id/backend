package usecase

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"os"

	"github.com/gabriel-vasile/mimetype"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
)

const MaxFileSize = 5 * 1024 * 1024 * 1024

type FileUseCase struct {
	fileRepo repositories.FileRepository
}

func NewFileUseCase(fileRepo repositories.FileRepository) FileUseCase {
	return FileUseCase{
		fileRepo: fileRepo,
	}
}

// removeFileUntilSuccess() deletes file from storage
func removeFileUntilSuccess(file *multipart.FileHeader) {
	err := os.Remove(file.Header.Get("path"))
	for err != nil {
		// if fail, try again
		err = os.Remove(file.Header.Get("path"))
	}
}

func (uc FileUseCase) Create(
	// we are only using fiber.Ctx to save the received file
	// doing this is kinda bad, imo
	c *fiber.Ctx,
	parentId *uuid.UUID,
	files []*multipart.FileHeader,
	userId uuid.UUID,
	uploadDir string,
) ([]string, []*models.File, *helpers.HttpError) {
	var isFolder bool
	var err error

	sz, _ := uc.fileRepo.GetUserFileSize(userId)
	// for now, hardcode maximum file size
	szTotal := sz
	for _, f := range files {
		szTotal += uint(f.Size)
	}
	if szTotal >= MaxFileSize {
		return nil, nil, &helpers.HttpError{
			Code:    http.StatusForbidden,
			Message: "Maksimum ukuran file total adalah 5GB",
		}
	}

	if parentId != nil {
		if isFolder, err = uc.fileRepo.GetIsFolderById(parentId); err != nil {
			return nil, nil, &helpers.HttpError{
				Code:    http.StatusNotFound,
				Message: "Folder dengan parent ID yang diberikan tidak ditemukan",
			}
		}
		if !isFolder {
			return nil, nil, &helpers.HttpError{
				Code:    http.StatusBadRequest,
				Message: "Folder dengan parent ID yang diberikan adalah file",
			}
		}
	}

	successfulSavesHeaders := make([]*multipart.FileHeader, 0)
	successfulSaves := make([]*models.File, 0)
	failedSavesName := make([]string, 0)

	// save to storage
	for _, file := range files {
		var savedFile *multipart.FileHeader
		if savedFile, err = helpers.SaveFile(c, file, uploadDir); err != nil {
			failedSavesName = append(failedSavesName, savedFile.Filename)
		} else {
			successfulSavesHeaders = append(successfulSavesHeaders, savedFile)
		}
	}

	// for each succesful save to storage, save to db
	for _, file := range successfulSavesHeaders {
		// get MIME type
		fileMime, _ := mimetype.DetectFile(file.Header.Get("path"))
		// add to DB
		fileId, _ := uuid.NewUUID()
		url := fmt.Sprintf("/files/%s", fileId.String())
		originalFileName := file.Header.Get("original_name")
		if len(originalFileName) > 255 {
			removeFileUntilSuccess(file)
		}

		savedName, err := helpers.SanitizeFileName(uc.fileRepo, parentId, &userId, originalFileName, false)
		var fileDb *models.File
		if fileDb, err = uc.fileRepo.Create(
			savedName,
			fileMime.String(),
			url,
			file.Filename,
			file.Size,
			false,
			&fileId,
			&userId,
			parentId,
		); err != nil {
			failedSavesName = append(failedSavesName, file.Header.Get("original_name"))
			removeFileUntilSuccess(file)
			continue
		}

		// add to list of successful names
		successfulSaves = append(successfulSaves, fileDb)
	}

	return failedSavesName, successfulSaves, nil
}

func (uc FileUseCase) Delete(
	fileId, userId uuid.UUID,
	uploadDir string,
	recursive bool,
) *helpers.HttpError {
	file, err := uc.fileRepo.GetFileById(&fileId)

	if err != nil {
		return &helpers.HttpError{
			Code:    http.StatusNotFound,
			Message: "File tidak ditemukan",
		}
	}

	if file.UserID != userId {
		return &helpers.HttpError{
			Code:    http.StatusUnauthorized,
			Message: "Tidak berhak menghapus file",
		}
	}

	if file.IsFolder && !recursive {
		return &helpers.HttpError{
			Code:    http.StatusPreconditionFailed,
			Message: "Bukan file",
		}
	} else if file.IsFolder && recursive {
		// delete folder
		children, _ := uc.fileRepo.GetByUserAndParentId(&userId, &fileId, "")
		for _, child := range *children {
			// INFO: we can assume there are no other users' file/folder
			// in the current repo, so we don't need to check for
			// ownership/permission
			uc.Delete(child.ID, userId, uploadDir, true)
		}
	} else if !file.IsFolder {
		// delete file
		err = os.Remove(fmt.Sprintf("%s/%s", uploadDir, file.SavedFileName))
		if err != nil {
			return &helpers.HttpError{
				Code:    http.StatusInternalServerError,
				Message: "Gagal menghapus file",
			}
		}
	}
	_ = uc.fileRepo.DeleteById(fileId)

	return nil
}

func (uc FileUseCase) GetPath(
	fileId *uuid.UUID,
	userId *uuid.UUID,
	uploadDir string,
) (string, *helpers.HttpError) {
	fileDbData, err := uc.fileRepo.GetFileById(fileId)
	if err != nil {
		return "", &helpers.HttpError{
			Code:    http.StatusNotFound,
			Message: "File tidak ditemukan",
		}
	}
	if fileDbData.IsFolder {
		return "", &helpers.HttpError{
			Code:    http.StatusPreconditionFailed,
			Message: "Bukan file",
		}
	}

	if !fileDbData.IsShared && fileDbData.UserID != *userId {
		return "", &helpers.HttpError{
			Code:    http.StatusUnauthorized,
			Message: "Anda tidak memiliki hak akses ke file tersebut",
		}
	}

	path := fmt.Sprintf("%s/%s", uploadDir, fileDbData.SavedFileName)

	return path, nil
}
