package usecase

import (
	"archive/zip"
	"net/http"
	"os"

	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
)

type FolderUseCase struct {
	fileRepo repositories.FileRepository
}

func NewFolderUseCase(folderRepo repositories.FileRepository) FolderUseCase {
	return FolderUseCase{
		fileRepo: folderRepo,
	}
}

func (uc FolderUseCase) Zip(folderId, userId *uuid.UUID, tempDir, uploadDir string) (string, *helpers.HttpError) {
	folder, err := uc.fileRepo.GetFileById(folderId)
	if err != nil {
		return "", &helpers.HttpError{
			Code:    http.StatusNotFound,
			Message: "File tidak ditemukan",
			Data:    nil,
		}
	}

	if !folder.IsShared && folder.UserID != *userId {
		return "", &helpers.HttpError{
			Code:    http.StatusUnauthorized,
			Message: "Bukan folder Anda",
			Data:    nil,
		}
	}

	var zipFile *os.File
	if zipFile, err = os.CreateTemp(tempDir, "download-*.zip"); err != nil {
		return "", &helpers.HttpError{
			Code:    http.StatusInternalServerError,
			Message: "Gagal membuat zip",
			Data:    nil,
		}
	}
	defer zipFile.Close()

	zipWriter := zip.NewWriter(zipFile)
	defer zipWriter.Close()

	files, _ := uc.fileRepo.GetByUserAndParentId(userId, folderId, "")
	err = helpers.AddToZip(zipWriter, files, folder.Name, uploadDir, &uc.fileRepo)
	if err != nil {
		return "", &helpers.HttpError{
			Code:    http.StatusInternalServerError,
			Message: "Gagal membuat zip",
			Data:    nil,
		}
	}

	return zipFile.Name(), nil
}
