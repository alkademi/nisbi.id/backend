package modules

import (
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

func Setup(app *fiber.App, db *gorm.DB) error {
	if _, err := newUserModule(app, db); err != nil {
		return err
	}
	if _, err := newForgotPasswordTokenModule(app, db); err != nil {
		return err
	}
	if _, err := newFileModule(app, db); err != nil {
		return err
	}
	if _, err := newFolderModule(app, db); err != nil {
		return err
	}
	if _, err := newFavoriteModule(app, db); err != nil {
		return err
	}
	newExampleModule(app)

	return nil
}
