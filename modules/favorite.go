package modules

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/controller"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gorm.io/gorm"
)

type favoriteModule struct {
	favoriteRepository repositories.FavoriteRepository
	favoriteController controller.FavoriteController
}

func newFavoriteModule(app *fiber.App, db *gorm.DB) (favoriteModule, error) {
	if err := db.AutoMigrate(&models.Favorite{}); err != nil {
		return favoriteModule{}, err
	}
	repo := repositories.NewFavoriteRepository(db)
	return favoriteModule{
		favoriteRepository: repo,
		favoriteController: controller.NewFavoriteController(app, repo),
	}, nil
}
