package modules

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/controller"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
	"gorm.io/gorm"
)

type forgotPasswordTokenModule struct {
	forgotPasswordTokenRepository repositories.ForgotPasswordTokenRepository
	forgotPasswordTokenController controller.ForgotPasswordTokenController
}

func newForgotPasswordTokenModule(app *fiber.App, db *gorm.DB) (forgotPasswordTokenModule, error) {
	if err := db.AutoMigrate(&models.ForgotPasswordToken{}); err != nil {
		return forgotPasswordTokenModule{}, err
	}
	repo := repositories.NewForgotPasswordTokenRepository(db)
	return forgotPasswordTokenModule{
		forgotPasswordTokenRepository: repo,
		forgotPasswordTokenController: controller.NewForgotPasswordTokenController(app, repo),
	}, nil
}
