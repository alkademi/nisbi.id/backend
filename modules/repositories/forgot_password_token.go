package repositories

import (
	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gorm.io/gorm"
)

type ForgotPasswordTokenRepository struct {
	db *gorm.DB
}

func NewForgotPasswordTokenRepository(db *gorm.DB) ForgotPasswordTokenRepository {
	return ForgotPasswordTokenRepository{
		db: db,
	}
}

func (repo ForgotPasswordTokenRepository) FindByToken(token string) (models.ForgotPasswordToken, error) {
	var forgotPasswordToken models.ForgotPasswordToken
	result := repo.db.Where("value = ?", token).First(&forgotPasswordToken)
	return forgotPasswordToken, result.Error
}

func (repo ForgotPasswordTokenRepository) FindByUserID(userId uuid.UUID) (models.ForgotPasswordToken, error) {
	var forgotPasswordToken models.ForgotPasswordToken
	result := repo.db.Where("user_id = ?", userId).First(&forgotPasswordToken)
	return forgotPasswordToken, result.Error
}

func (repo ForgotPasswordTokenRepository) Create(token string, userId uuid.UUID) (models.ForgotPasswordToken, error) {
	forgotPasswordToken := models.ForgotPasswordToken{
		Value:  token,
		UserId: userId,
	}
	result := repo.db.Create(&forgotPasswordToken)
	return forgotPasswordToken, result.Error
}

func (repo ForgotPasswordTokenRepository) DeleteOne(tokenId uuid.UUID) (models.ForgotPasswordToken, error) {
	var forgotPasswordToken models.ForgotPasswordToken
	result := repo.db.Delete(&forgotPasswordToken, tokenId)
	return forgotPasswordToken, result.Error
}
