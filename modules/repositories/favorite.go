package repositories

import (
	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gorm.io/gorm"
)

type FavoriteRepository struct {
	db *gorm.DB
}

func NewFavoriteRepository(db *gorm.DB) FavoriteRepository {
	return FavoriteRepository{
		db: db,
	}
}

func (repo FavoriteRepository) Create(userId, fileId uuid.UUID) (models.Favorite, error) {
	favorite := models.Favorite{
		UserID: userId,
		FileID: fileId,
	}

	result := repo.db.Create(&favorite)
	return favorite, result.Error
}

func (repo FavoriteRepository) GetFavoriteById(id uuid.UUID) (models.Favorite, error) {
	var favorite models.Favorite
	result := repo.db.Where("id = ?", id).Preload("File").First(&favorite)
	return favorite, result.Error
}

func (repo FavoriteRepository) GetFavoritesByUserId(id uuid.UUID, loadFile bool) ([]models.Favorite, error) {
	var favorites []models.Favorite
	if loadFile {
		result := repo.db.Where("user_id = ?", id).Preload("File").Find(&favorites)
		return favorites, result.Error
	} else {
		result := repo.db.Where("user_id = ?", id).Find(&favorites)
		return favorites, result.Error
	}
}

func (repo FavoriteRepository) GetSingleFavorite(userId, fileId uuid.UUID) (models.Favorite, error) {
	var favorite models.Favorite
	result := repo.db.Where("user_id = ?", userId).Where("file_id = ?", fileId).First(&favorite).Joins("File")
	return favorite, result.Error
}

func (repo FavoriteRepository) DeleteOne(id uuid.UUID) (models.Favorite, error) {
	var favorite models.Favorite
	result := repo.db.Unscoped().Delete(&favorite, id)
	return favorite, result.Error
}

func (repo FavoriteRepository) ResetThirdPersonFavorites(ownerId uuid.UUID, fileId uuid.UUID) (models.Favorite, error) {
	var favorite models.Favorite
	result := repo.db.Where("file_id = ?", fileId).Not("user_id = ?", ownerId).Unscoped().Delete(&favorite)
	return favorite, result.Error
}
