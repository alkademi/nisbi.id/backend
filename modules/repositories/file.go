package repositories

import (
	"fmt"

	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gorm.io/gorm"
)

type FileRepository struct {
	db *gorm.DB
}

func NewFileRepository(db *gorm.DB) FileRepository {
	return FileRepository{
		db: db,
	}
}

func (repo FileRepository) Create(name, mimeType, url, savedFileName string, size int64, isFolder bool, id, userId, parentFolderId *uuid.UUID) (*models.File, error) {
	file := models.File{
		BaseNoDeletedAt: models.BaseNoDeletedAt{
			ID: *id,
		},
		Name:           name,
		UserID:         *userId,
		Size:           size,
		MimeType:       mimeType,
		IsShared:       false,
		Url:            url,
		IsFolder:       isFolder,
		ParentFolderID: parentFolderId,
		SavedFileName:  savedFileName,
	}

	result := repo.db.Create(&file)

	return &file, result.Error
}

func (repo FileRepository) GetIsFolderById(id *uuid.UUID) (bool, error) {
	var file models.File
	result := repo.db.Where("id = ?", id).Select("is_folder").First(&file)

	return file.IsFolder, result.Error
}

func (repo FileRepository) GetFileById(id *uuid.UUID) (*models.File, error) {
	var file models.File
	result := repo.db.Where("id = ?", id).First(&file)

	return &file, result.Error
}

func (repo FileRepository) GetByUserAndParentId(userId, parentId *uuid.UUID, searchQuery string) (*[]models.File, error) {
	var files []models.File
	var result *gorm.DB

	if searchQuery != "" {
		baseWhereQuery := "(user_id = ? OR is_shared = TRUE) AND name ILIKE ?"
		result = repo.db.Where(baseWhereQuery, userId, "%"+searchQuery+"%").Preload("User").Find(&files)
	} else {
		baseWhereQuery := "user_id = ?"
		if parentId == nil {
			result = repo.db.Where(fmt.Sprintf("%s AND parent_folder_id IS NULL", baseWhereQuery), userId).Preload("User").Find(&files)
		} else {
			result = repo.db.Where(fmt.Sprintf("%s AND parent_folder_id = ?", baseWhereQuery), userId, parentId).Preload("User").Find(&files)
		}
	}

	return &files, result.Error
}

func (repo FileRepository) UpdateOne(id uuid.UUID, newFile *models.File) (models.File, error) {
	var file models.File
	result := repo.db.Where("id =?", id).Model(&file).Updates(map[string]interface{}{"name": newFile.Name, "is_shared": newFile.IsShared, "parent_folder_id": newFile.ParentFolderID})
	return file, result.Error
}

func (repo FileRepository) DeleteById(id uuid.UUID) error {
	return (repo.db.Delete(&models.File{}, id)).Error
}

func (repo FileRepository) GetUserFileSize(userId uuid.UUID) (uint, error) {
	var sum uint
	result := repo.db.
		Table("files").
		Select("sum(size)").
		Where("user_id = ?", userId).
		Scan(&sum)

	return sum, result.Error
}

func (repo FileRepository) GetByIds(id []*uuid.UUID) ([]*models.File, error) {
	files := make([]*models.File, 0)

	res := repo.db.Find(&files, id)

	return files, res.Error
}

func (repo FileRepository) GetParent(file *models.File) error {
	file.Parent = &models.File{}
	result := repo.db.Where("id = ?", file.ParentFolderID).First(file.Parent)

	return result.Error
}
