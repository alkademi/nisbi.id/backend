package repositories

import (
	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gorm.io/gorm"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return UserRepository{
		db: db,
	}
}

func (repo UserRepository) FindByID(userId uuid.UUID) (models.User, error) {
	var user models.User
	result := repo.db.Where("id = ?", userId).First(&user)
	return user, result.Error
}

func (repo UserRepository) FindByEmail(email string) (models.User, error) {
	var user models.User
	result := repo.db.Where("email = ?", email).First(&user)
	return user, result.Error
}

func (repo UserRepository) Create(name, email, password, validationToken string) (models.User, error) {
	user := models.User{
		Name:            name,
		Email:           email,
		Password:        password,
		ValidationToken: validationToken,
	}
	result := repo.db.Create(&user)
	return user, result.Error
}

func (repo UserRepository) UpdateOne(id uuid.UUID, newUser *models.User) (models.User, error) {
	var user models.User
	result := repo.db.Where("id =?", id).Model(&user).Updates(newUser)
	return user, result.Error
}
