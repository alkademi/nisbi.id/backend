# nisbi.id

## Setup
1. jalankan `go mod tidy`
1. salin `.env.example` ke `.env` dan ubah nilai-nilainya
1. pada console PostgreSQL jalankan: `CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`

## Dev

1. jalankan `go run main.go`

## Testing

1. jalankan `go get github.com/onsi/ginkgo/v2/ginkgo` dan `go get github.com/onsi/gomega/...` untuk menambah package ginkgo dan gomega
2. jalankan `go mod tidy` kembali jika diperlukan
3. untuk melaksanakan unit testing, start server dengan `go run main.go` lalu jalankan `go test -v ./tests/unit_testing`
4. untuk melaksanakan database testing, jalankan `ginkgo -r ./tests/db_testing`

## Framework yang Digunakan

- [Fiber](https://docs.gofiber.io/)
- [Gorm](https://gorm.io/index.html)
