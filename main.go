package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"gitlab.informatika.org/k03_34_nisbi/backend/helpers"
	"gitlab.informatika.org/k03_34_nisbi/backend/middlewares"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules"
)

func main() {
	godotenv.Load()

	port := os.Getenv("PORT")
	prodMode := os.Getenv("ENV") == "production"
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	db, err := helpers.ConnectDb(dbHost, dbPort, dbUser, dbPass, dbName, !prodMode)
	if err != nil {
		log.Fatal(err)
	}

	app := fiber.New(fiber.Config{
		BodyLimit: 50 * 1024 * 1024, // 50 MB request limit
	})
	middlewares.Setup(app, prodMode)
	if err = modules.Setup(app, db); err != nil {
		log.Fatal(err)
	}

	if port == "" {
		port = "8080"
	}
	log.Fatal(app.Listen(fmt.Sprintf(":%s", port)))
}
