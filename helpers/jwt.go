package helpers

import (
	"fmt"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"github.com/mitchellh/mapstructure"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
)

type UserData struct {
	Id        uuid.UUID
	Name      string
	Email     string
	Validated bool
}

func GenerateJWT(user models.User) (string, error) {
	godotenv.Load()
	SigningKey := []byte(os.Getenv("SIGNING_KEY"))

	userData := new(UserData)

	userData.Id = user.ID
	userData.Name = user.Name
	userData.Email = user.Email
	userData.Validated = !user.ValidatedAt.IsZero()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"authorized": true,
		"user":       userData,
		"exp":        time.Now().Add(time.Hour * 24 * 7).Unix(), // 1 week
	})

	tokenString, err := token.SignedString(SigningKey)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

type TokenData struct {
	authorized bool
	exp        time.Time
	user       UserData
}

func ParseJWT(userToken string) (UserData, error) {
	godotenv.Load()
	SigningKey := []byte(os.Getenv("SIGNING_KEY"))

	token, err := jwt.Parse(userToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return SigningKey, nil
	})

	userData := UserData{}

	if err != nil {
		return userData, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		user := claims["user"].(map[string]interface{})
		id := user["Id"].(string)

		mapstructure.Decode(claims["user"], &userData)
		userData.Id = uuid.MustParse(id)

		return userData, nil
	}

	return userData, err
}

func GetUserData(c *fiber.Ctx) UserData {
	data := c.Locals("user")
	userData := new(UserData)

	mapstructure.Decode(data, &userData)

	return *userData
}
