package helpers

import "encoding/json"

type response struct {
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

// Fungsi untuk membuat response HTTP
// sebuah JSON yang terdiri atas 2 keys: data dan message
// Returns marshal dari JSON atau error yang terjadi
func GenerateResponse(data interface{}, message string) ([]byte, error) {
	resp := response{
		Data:    data,
		Message: message,
	}

	return json.Marshal(resp)
}
