package helpers

import (
	"archive/zip"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/models"
	"gitlab.informatika.org/k03_34_nisbi/backend/modules/repositories"
)

// SaveFile akan menambahakna sebuah file yang diupload menggunakan Fiber ke storage
// c context fiber
// file header untuk file yang mau disave
// uploadDir directory tempat menyimpan file
func ValidateFolder(fileRepo repositories.FileRepository, folderId *uuid.UUID, userId uuid.UUID) (*models.File, error) {
	var folder *models.File
	folder, err := fileRepo.GetFileById(folderId)
	if err != nil || folder.UserID != userId {
		resp := errors.New("Folder dengan parent ID yang diberikan tidak ditemukan")
		return nil, resp
	}
	if !folder.IsFolder {
		resp := errors.New("Folder dengan parent ID yang diberikan adalah file")
		return nil, resp
	}
	return folder, nil
}

func SanitizeFileName(fileRepo repositories.FileRepository, parentId *uuid.UUID, userId *uuid.UUID, name string, isCopy bool) (string, error) {
	filesCombined, err := fileRepo.GetByUserAndParentId(userId, parentId, "")

	if err != nil {
		return "", err
	}

	fileName := strings.Split(name, ".")
	rawName, extension := fileName[0], ""
	if len(fileName) > 1 {
		extension = "." + fileName[1]
	}

	targetName := rawName
	if isCopy {
		targetName = "Copy of " + targetName
	}

	copyNumbers := make([]int, 0) // listing all copy number with the same rawname
	isSameRawNameExist := false   // checking if there's any same raw name
	isFirstCopyExist := false     // checking if "Copy of name exist"

	// checking if there's any file with existing name/copy exist
	for _, file := range *filesCombined {
		if isCopy && !isFirstCopyExist && file.Name == "Copy of "+name { //if copy of name exist
			isFirstCopyExist = true
			copyNumbers = append(copyNumbers, 1)
		}

		if !isSameRawNameExist && file.Name == name { //if same raw name exist
			isSameRawNameExist = true
			copyNumbers = append(copyNumbers, 1)
		}

		if isSameRawNameExist && isFirstCopyExist {
			break
		}
	}

	if isSameRawNameExist { //let's count the number..
		for _, file := range *filesCombined {
			openParIdx := -1
			closeParIdx := -1

			n := len(file.Name)

			for i := n - 1; i >= 0 && (openParIdx == -1 || closeParIdx == -1); i-- {
				if openParIdx == -1 && file.Name[i] == '(' {
					openParIdx = i
				}
				if closeParIdx == -1 && file.Name[i] == ')' {
					closeParIdx = i
				}
			}

			if openParIdx < closeParIdx && openParIdx != -1 && closeParIdx != -1 {
				targetNumber := string(file.Name[openParIdx+1 : closeParIdx])
				number, err := strconv.Atoi(targetNumber)

				if err == nil {
					fileRawName := file.Name[0 : openParIdx-1] //remove space before (

					if fileRawName == targetName {
						copyNumbers = append(copyNumbers, number)
					}
				}
			}
		}
	}

	sort.Ints(copyNumbers)
	lastNumber := 0
	for _, num := range copyNumbers {
		if num > lastNumber+1 && lastNumber != 0 { //if there's any empty number
			break
		}
		lastNumber = num
	}

	if !isCopy {
		if lastNumber == 0 && !isSameRawNameExist {
			return rawName + extension, nil
		}
		lastNumber += 1
	} else if lastNumber == 1 && !isFirstCopyExist { // if only the original file existed
		return "Copy of " + rawName + extension, nil
	} else { // if any copy existing
		lastNumber += 1
	}

	targetName = rawName + " (" + strconv.Itoa(lastNumber) + ")" + extension
	if isCopy {
		targetName = "Copy of " + targetName
	}

	return targetName, nil
}

func BulkToggleShare(fileRepo repositories.FileRepository, parent_id *uuid.UUID, userId *uuid.UUID, isShare bool) error {
	filesInFolder, err := fileRepo.GetByUserAndParentId(userId, parent_id, "")

	if err != nil {
		return err
	}

	for _, file := range *filesInFolder {
		file.IsShared = isShare
		_, err := fileRepo.UpdateOne(file.ID, &file)

		if err != nil {
			return err
		}

		if file.IsFolder {
			err := BulkToggleShare(fileRepo, &file.ID, userId, isShare)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// SaveFile akan menambahakna sebuah file yang diupload menggunakan Fiber ke storage
// c context fiber
// file header untuk file yang mau disave
// uploadDir directory tempat menyimpan file
func SaveFile(c *fiber.Ctx, file *multipart.FileHeader, uploadDir string) (*multipart.FileHeader, error) {
	// create file name
	fileName := fmt.Sprintf("%d_%s", time.Now().UnixNano(), file.Filename)
	// create path to file relative to where app.go started
	filePath := fmt.Sprintf("%s/%s", uploadDir, fileName)
	if errSave := c.SaveFile(file, filePath); errSave != nil {
		// kalau gagal save 1 file, delete file tersebut
		errNested := os.Remove(filePath)
		// coba lagi pas error
		// I know this is might be bad, but I am tired. HMU or create a
		// PR if you have a better idea
		for errNested != nil {
			errNested = os.Remove(filePath)
		}
		return nil, errSave
	}

	// tambahin file ke list file yang udah disave
	savedFile := *file
	savedFile.Filename = fileName
	savedFile.Header.Add("path", filePath)
	savedFile.Header.Add("original_name", file.Filename)
	return &savedFile, nil
}

func AddToZip(
	zipWriter *zip.Writer,
	files *[]models.File,
	parentName,
	uploadDir string,
	fileRepo *repositories.FileRepository,
) (err error) {
	for _, fileModel := range *files {
		if fileModel.IsFolder {
			// kalo folder, lakukan recursion
			tmpParentName := parentName + "/" + fileModel.Name
			tmpFiles, _ := (*fileRepo).GetByUserAndParentId(&fileModel.UserID, &fileModel.ID, "")
			err = AddToZip(zipWriter, tmpFiles, tmpParentName, uploadDir, fileRepo)
			if err != nil {
				return err
			}
		} else {
			// kalo file, tambah ke zip
			path := fmt.Sprintf("%s/%s", uploadDir, fileModel.SavedFileName)
			var file *os.File
			if file, err = os.Open(path); err != nil {
				return errors.New("1")
			}

			shortFilename := strings.Join(
				strings.Split(fileModel.SavedFileName, "_")[1:], "_",
			)
			filenameShort := parentName + "/" + shortFilename

			info, _ := file.Stat()
			header, _ := zip.FileInfoHeader(info)
			header.Name = filenameShort
			header.Method = zip.Deflate

			var writer io.Writer
			if writer, err = zipWriter.CreateHeader(header); err != nil {
				return errors.New("2")
			}
			_, err = io.Copy(writer, file)
		}
	}

	return nil
}
