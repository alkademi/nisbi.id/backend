package helpers

import (
	"net/smtp"
	"os"

	"github.com/joho/godotenv"
)

func SetupEmailAuth() (host string, from string, password string, address string) {
	godotenv.Load()
	from = os.Getenv("MAIL_FROM")
	password = os.Getenv("MAIL_PASSWORD")

	host = "smtp.gmail.com"
	port := "587"

	address = host + ":" + port

	return host, from, password, address
}

func SendResetPasswordToken(token string, targetEmail string) error {
	host, from, password, address := SetupEmailAuth()

	message := []byte("To: " + targetEmail + "\r\n" +
		"Subject: [NISBI] Lupa Sandi\r\n" +
		"\r\n" +
		"Masukan token berikut untuk melanjutkan reset password \r\n" + token)

	to := []string{targetEmail}

	auth := smtp.PlainAuth("", from, password, host)
	err := smtp.SendMail(address, auth, from, to, message)
	return err
}

func SendValidationEmail(token string, targetEmail string) error {
	host, from, password, address := SetupEmailAuth()

	message := []byte("To: " + targetEmail + "\r\n" +
		"Subject: [NISBI] Token Validasi Akun\r\n" +
		"\r\n" +
		"Masukan token berikut untuk mengaktifkan akun anda \r\n" + token)

	to := []string{targetEmail}

	auth := smtp.PlainAuth("", from, password, host)
	err := smtp.SendMail(address, auth, from, to, message)
	return err
}
