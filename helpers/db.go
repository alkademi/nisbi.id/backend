package helpers

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var dbConn *gorm.DB

// Fungsi untuk menghubungkan ke PostgreSQL
// devMode akan bernilai false jika mode environment adalah "production"
func ConnectDb(host string, port string, user string, password string, dbName string, devMode bool) (*gorm.DB, error) {
	if dbConn == nil {
		dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s TimeZone=UTC", host, port, user, password, dbName)
		logLevel := logger.Silent
		if devMode {
			logLevel = logger.Info
		}
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
			Logger: logger.Default.LogMode(logLevel),
		})
		if err != nil {
			return nil, err
		}
		dbConn = db
	}
	return dbConn, nil
}
