package helpers

import "fmt"

type HttpError struct {
	Code    int
	Message string
	Data    interface{}
}

func (he HttpError) Error() string {
	return fmt.Sprintf("HTTP Error (%d): %s", he.Code, he.Message)
}
